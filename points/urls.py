from django.urls import path, include
from .views import *

urlpatterns = [
    path('task/add/', TaskCreate.as_view(), name = 'task-add'),
    path('task/<int:pk>/edit', TaskUpdate.as_view(), name = 'task-update'),
    path('task/<int:pk>/', TaskUpdate.as_view(), name = 'task-detail'),
    path('task/<int:pk>/delete', TaskDelete.as_view(), name = 'task-delete'),
    path('task/', TaskList.as_view(), name = 'task-list'),

    path('point/add/', PointCreate.as_view(), name = 'point-add'),
    path('point/<int:pk>/edit', PointUpdate.as_view(), name = 'point-update'),
    path('point/<int:pk>/', PointUpdate.as_view(), name = 'point-detail'),
    path('point/<int:pk>/delete', PointDelete.as_view(), name = 'point-delete'),
    path('point/', PointList.as_view(), name = 'point-list'),

    path('redemption/add/', RedemptionCreate.as_view(), name = 'redemption-add'),
    path('redemption/<int:pk>/edit', RedemptionUpdate.as_view(), name = 'redemption-update'),
    path('redemption/<int:pk>/', RedemptionUpdate.as_view(), name = 'redemption-detail'),
    path('redemption/<int:pk>/delete', RedemptionDelete.as_view(), name = 'redemption-delete'),
    path('redemption/', RedemptionList.as_view(), name = 'redemption-list'),

    path('summary/', LandingPage.as_view(), name='summary'),
]
