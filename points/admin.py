from django.contrib import admin
from points.models import *

# Register your models here.
admin.site.register(Task)
admin.site.register(Point)
admin.site.register(Redemption)
