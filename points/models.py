from django.db import models
from django.urls import reverse

# Create your models here.
class Task(models.Model):
    task_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    value = models.IntegerField()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('task-detail', kwargs={'pk': self.pk})

class Point(models.Model):
    point_id = models.AutoField(primary_key=True)
    earned_dt = models.DateField()
    task = models.ForeignKey(Task, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['-earned_dt']

    def __str__(self):
        return self.task.name + ' on ' + str(self.earned_dt)

    def get_absolute_url(self):
        return reverse('point-detail', kwargs={'pk': self.pk})

class Redemption(models.Model):
    redemption_id = models.AutoField(primary_key=True)
    redeemed_dt = models.DateField()
    name = models.CharField(max_length=100)
    cost = models.DecimalField(max_digits=5, decimal_places=2)

    class Meta:
        ordering = ['-redeemed_dt']

    def __str__(self):
        return self.name + ' ($' + str(self.cost) + ') on ' + str(self.redeemed_dt)

    def get_absolute_url(self):
        return reverse('redemption-detail', kwargs={'pk': self.pk})
