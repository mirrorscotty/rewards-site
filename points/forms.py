from django import forms
from points.models import *

class RedemptionForm(forms.ModelForm):
    class Meta:
        model = Redemption
        fields = ['name', 'cost', 'redeemed_dt']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'cost': forms.NumberInput(attrs={'class': 'form-control'}),
            'redeemed_dt': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
        }

class PointForm(forms.ModelForm):
    class Meta:
        model = Point
        fields = ['earned_dt', 'task']
        widgets = {
            'task': forms.Select(attrs={'class': 'form-control'}),
            'earned_dt': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
        }

class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['name', 'value']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'value': forms.NumberInput(attrs={'class': 'form-control'}),
        }
