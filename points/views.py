from django.shortcuts import render
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from points.models import *
from points.forms import *
from django.urls import reverse_lazy
from django.db.models import Sum, F
from django.contrib.auth.mixins import LoginRequiredMixin

from datetime import date

# Create your views here.
class TaskCreate(LoginRequiredMixin, CreateView):
    model = Task
    form_class = TaskForm
    success_url = reverse_lazy('task-list')
    template_name = 'base/base_form.html'

class TaskUpdate(LoginRequiredMixin, UpdateView):
    model = Task
    form_class = TaskForm
    success_url = reverse_lazy('task-list')
    template_name = 'base/base_update_form.html'

class TaskDelete(LoginRequiredMixin, DeleteView):
    model = Task
    success_url = reverse_lazy('task-list')
    template_name = 'base/base_confirm_delete.html'

class TaskList(LoginRequiredMixin, ListView):
    model = Task

class TaskDetail(LoginRequiredMixin, DetailView):
    model = Task

class PointCreate(LoginRequiredMixin, CreateView):
    model = Point
    form_class = PointForm
    success_url = reverse_lazy('point-list')
    template_name = 'base/base_form.html'

class PointUpdate(LoginRequiredMixin, UpdateView):
    model = Point
    form_class = PointForm
    success_url = reverse_lazy('point-list')
    template_name = 'base/base_form.html'

    def get_initial(self, *args, **kwargs):
        initial = super().get_initial().copy()
        initial['earned_dt'] = date.today()
        return initial

class PointDelete(LoginRequiredMixin, DeleteView):
    model = Point
    success_url = reverse_lazy('point-list')
    template_name = 'base/base_confirm_delete.html'

class PointList(LoginRequiredMixin, ListView):
    model = Point

class PointDetail(LoginRequiredMixin, DetailView):
    model = Point

class RedemptionCreate(LoginRequiredMixin, CreateView):
    model = Redemption
    form_class = RedemptionForm
    success_url = reverse_lazy('redemption-list')
    template_name = 'base/base_form.html'

class RedemptionUpdate(LoginRequiredMixin, UpdateView):
    def get_initial(self, *args, **kwargs):
        initial = super().get_initial().copy()
        initial['redeemed_dt'] = date.today()
        return initial

    model = Redemption
    form_class = RedemptionForm
    success_url = reverse_lazy('redemption-list')
    template_name = 'base/base_update_form.html'

class RedemptionDelete(LoginRequiredMixin, DeleteView):
    model = Redemption
    success_url = reverse_lazy('redemption-list')
    template_name = 'base/base_confirm_delete.html'

class RedemptionList(LoginRequiredMixin, ListView):
    model = Redemption

class RedemptionDetail(LoginRequiredMixin, DetailView):
    model = Redemption

class LandingPage(LoginRequiredMixin, TemplateView):
    template_name = 'points/landing_page.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        total_dollars = Point.objects.aggregate(Sum('task__value'))['task__value__sum']
        total_spent = Redemption.objects.aggregate(Sum('cost'))['cost__sum']
        if total_dollars is None:
            total_dollars = 0.0
        if total_spent is None:
            total_spent = 0.0

        context['total_dollars'] = total_dollars
        context['total_spent'] = total_spent
        context['total_points'] = Point.objects.count()

        context['dollars_remaining'] = total_dollars - total_spent

        return context
